
---
layout: "post"
title: "Fediverse.Party turns 5"
date: 2022-11-26
updated: 2022-11-26
tags:
    - fediverse
preview: "Can you believe it's been five years already? Five years of keeping up to date with all the diverse projects that comprise Fediverse."
url: "/en/post/fediverse-party-turns-five"
lang: en
authors: [{"name": "@lostinlight", "url": "https://mastodon.xyz/@lightone", "network": "mastodon"}]
---

### Fediverse.party turns 5

Can you believe it's been five years already? Five years of helping novices get
their bearings in the varied landscape that is Fediverse. Five years of guiding people towards smaller, niche instances to strengthen the federation. Five years of keeping up to date with all the diverse projects, big and small, that comprise our network.

This was achieved in large part due to all your pull requests with corrections and updates. <span class="u-goldenBg">Thanks to all the contributors!</span> By the way, since moving to Codeberg, our repository is getting almost twice more pull requests than it used to get on GitLab. ;)

Here's to five more years! 🎉
