
---
layout: "post"
title: Update October 2023
date: 2023-10-30
updated: 2023-10-30
tags:
    - fediparty
preview:
  "First ever update on what's new on this website, and a big question for everyone who's still reading this blog or RSS feed"
url: "/en/post/update-october-2023"
lang: en
authors: [{"name": "@lostinlight", "url": "https://mastodon.xyz/@lightone", "network": "mastodon"}]
---

### Fediparty update, October 2023

Hi, fedizens! Long time no see.

*@lostinlight* here 👋, with a small announcement and a big question for everyone who's still reading this blog or RSS feed (is anyone out there? :)

Once upon a time *Fediverse.Party* tried to keep up with everything going on in Fedi. We posted about latest software releases and developments on the [Chronicles](https://fediverse.party/en/chronicles/) page, via RSS and Friendica account. But no new posts have appeared for a long time.

It's because there're **three great sources of Fediverse news** now: [fediversereport.com](https://fediversereport.com), [wedistribute.org](https://wedistribute.org), and [@weekinfediverse](https://mitra.social/@weekinfediverse). They cover all the stories happening in our federated universe. Following them is the best way to stay well-informed!

What shall happen to Chronicles page of this website then? Removing it would not be right; yearly Fediverse recaps and Birthday posts should remain at least for the sake of history. Now that we have a [Boosty](https://boosty.to/fediparty) page, I think it'll be useful to post about website updates on a somewhat regular basis. Like release notes, but for a website. I hope it'll help readers find out about new ActivityPub tools and Fediverse-related projects (even though some of the projects added to *Software* and *Developer tools* pages are not new, it just took a long time to find them).

So, here goes the summary of October site updates.

### Projects added to [Software](https://fediverse.party/en/miscellaneous/):

- **[Mbin](https://github.com/mbinorg/mbin)** – a fork of kbin, community-focused;
- **[Messy](https://github.com/rekcuFniarB/messy-fediverse)** – single user ActivityPub instance intended to add Fediverse compatibility to existing Django-based sites;
- **[SofaPub](https://gitlab.com/justindthomas/sofapub)** – a minimally functional ActivityPub implementation in Rust;
- **[Vidzy](https://codeberg.org/vidzy/vidzy)** – federated alternative to TikTok;
- **[LibRate](https://codeberg.org/mjh/LibRate)** – libre media rating website for the Fediverse.

<br>

### Projects added to [Developer tools](https://fediverse.party/en/tools/):

- **[GhostCMS ActivityPub](https://codeberg.org/quigs/ghostcms-activitypub)** – an ExpressJS server that integrates with GhostCMS webhooks to publish ActivityPub content on the Fediverse;
- **[Mobilizon Crossposter](https://github.com/gnh1201/gnuboard5-activitypub)** – a modular crossposter to bridge events from external sources to Mobilizon;
- **[M-OAuth](https://github.com/Sevichecc/M-OAuth)** – access token generator for Akkoma, Pleroma, Mastodon APIs;
- **[idkfa](https://humungus.tedunangst.com/r/idkfa/f)** – proxy designed to consolidate multiple AP actors; it presents a single unified activity interface to the outside world, while communicating with a cornucopia of internal servers;
- **[Hatsu](https://github.com/importantimport/hatsu)** – self-hosted and fully automated ActivityPub bridge for static sites;
- **[Fedipage](https://git.qoto.org/fedipage/fedipage)** –  Hugo based static page generator and blog with ActivityPub support;
- **[ActivityPub Test Suite](https://github.com/steve-bate/activitypub-testsuite)** – server-independent, full-automated test suite primary focused on ActivityPub server compliance testing;
- **[Lemmy Automoderator](https://github.com/basedcount/lemmy-automoderator)** – automated removal of Lemmy posts, comments based on title, content or link; user whitelisting and exceptions for moderators;
- **[Lemmy Migrate](https://github.com/wescode/lemmy_migrate)** – migrate your subscribed Lemmy communites to a new account;
- **[Lemmy Schedule](https://github.com/RikudouSage/LemmySchedule)** – app for scheduling posts, pins/unpins and notifications about new content in Lemmy;
- **[Fedi safety](https://github.com/db0/fedi-safety)** – script that goes through Lemmy images in storage and tries to prevent illegal or unethical content;
- **[FediFetcher](https://github.com/nanos/FediFetcher)** – tool for Mastodon that automatically fetches missing replies and posts from other Fediverse instances and adds them to your own Mastodon instance;
- **[GetMoarFediverse](https://github.com/g3rv4/GetMoarFediverse)** – import content into your instance that's tagged with hashtags you're interested in;
- **[FakeRelay](https://github.com/g3rv4/FakeRelay)** – an API to index statuses on Mastodon acting as a relay;
- **[masto-backfill](https://github.com/AdamK2003/masto-backfill)** – fetches old posts on your Mastodon, Pleroma or compatible instance(s);
- **[Analytodon](https://github.com/blazer82/analytodon)** – monitor follower growth, identify popular posts, track boosts, favorites, and much more; can be self-hosted;
- **[LASIM](https://github.com/CMahaff/lasim)** – move your Lemmy settings from one account to another;
- **[Pythörhead](https://github.com/db0/pythorhead)** – Python library for interacting with Lemmy;
- **[Granary](https://github.com/snarfed/granary)** – social web translator; it fetches and converts data between social networks, HTML and JSON, ActivityStreams/ActivityPub, and more;
- **[Combine.social](https://combine.social)** – combine remote and local timelines; pre-fetch all missing replies in your home timeline;
- **[ActivityColander](https://gitlab.com/babka_net/activitycolander)** –  Fediverse spam gateway, designed to keep unwanted messages from either reaching your ActivityPub server, or tagging them for handling later.

<br>

### Other improvements

There's a new **filter by license** on Software page. And Lemmy was added to the frontpage.

### UX research

Now comes the big question for all the readers of this blog and users of Fediparty website. We've been with you for more than 5 years, but never asked you how you're using this site. What are the pages you visit most often? What pages or features you find most useful? Which ones you find poorly designed?

Any ideas, suggestions, complaints, feedback you have, please, share with us! Here's a special [Codeberg issue](https://codeberg.org/fediverse/fediparty/issues/175) for it. Or you can write your suggestions as an answer to [this Mastodon post](https://mastodon.xyz/@lightone/111325495741837777).

Thanks in advance! 💜


